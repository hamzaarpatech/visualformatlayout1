//
//  ViewController.swift
//  visualFormatLayout1
//
//  Created by Hamza on 2/18/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label1 = UILabel()
        label1.translatesAutoresizingMaskIntoConstraints = false
        label1.text = "first"
        label1.backgroundColor = .red
        label1.sizeToFit()
        
        let label2 = UILabel()
        label2.translatesAutoresizingMaskIntoConstraints = false
        label2.text = "second"
        label2.backgroundColor = .cyan
        label2.sizeToFit()
        
        let label3 = UILabel()
        label3.translatesAutoresizingMaskIntoConstraints = false
        label3.text = "third"
        label3.backgroundColor = .green
        label3.sizeToFit()
        
        let label4 = UILabel()
        label4.translatesAutoresizingMaskIntoConstraints = false
        label4.text = "fourth"
        label4.backgroundColor = .darkGray
        label4.sizeToFit()
        
        view.addSubview(label1)
        view.addSubview(label2)
        view.addSubview(label3)
        view.addSubview(label4)
        
        let labelDictionary = ["label1":label1 , "label2":label2 , "label3":label3 , "label4":label4]

        for label in labelDictionary.keys{
           view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[\(label)]|", options: [], metrics: nil, views: labelDictionary))
        }
        
//        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-50-[label1(50@750)]-[label2(50@750)]-[label3(50@750)]-[label4(50@250)]-(>=50)-|", options: [], metrics: nil, views: labelDictionary))
        
//        var metrics = ["labelHeight" : 100]
//        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-50-[label1(labelHeight)]-[label2(labelHeight)]-[label3(labelHeight)]-[label4(labelHeight)]-(>=50)-|", options: [], metrics: metrics, views: labelDictionary))
        
         let metrics = ["labelHeight" : 100]
         view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-50-[label1(labelHeight@999)]-[label2(label1)]-[label3(label1)]-[label4(label1)]-(>=50)-|", options: [], metrics: metrics, views: labelDictionary))
       
    }


}

